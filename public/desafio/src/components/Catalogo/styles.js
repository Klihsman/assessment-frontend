import styled from 'styled-components';

export const CatalogoContainer = styled.div`
    min-height: 80vh;
    width: 100%;

    display: flex;
    justify-content: space-around;
    
    @media(max-width: 768px){
        flex-direction: column;
        align-items: center;
    }
`;

export const FilterContainer = styled.div`
    height: 40vh;
    width: 200px;


    border: 1px solid #ddd;

    margin-top: 2%;
    margin-right: 2%;

    h1{
        color: #bc1d2b;

        font-size: 1.3em;
        margin-top: 2%;
        margin-left: 5%;

        @media(max-width: 768px){
            font-size: 1.7em;
        }
    }

    h2{
        color: #b1c9c8;

        font-size: 1.1em;

        margin-top: 5%;
        margin-left: 5%;

        @media(max-width: 768px){
            font-size: 1.3em;
        }
    }

    ul{
        margin-left: 15%;
        margin-top: 2%;

        li{
            @media(max-width: 768px){
                font-size: 1.3em;
             }   

            :hover{
                cursor: pointer;
                text-decoration: underline;
            }
        }
    }

    h3{
        color: #b1c9c8;

        font-size: 1.1em;

        margin-top: 5%;
        margin-left: 5%;

        @media(max-width: 768px){
            font-size: 1.3em;
        }
    }   

    @media(max-width: 768px){
        width: 95%;
    }

`;

export const ProductsContainer = styled.div`
    min-height: 100%;
    width: 75%;

    display: flex;
    flex-direction: column;

    margin-top: 2%;

    h2{
        color: #bc1d2b;
    }

    @media(max-width: 768px){
        width: 95%;
            

    }
`;

export const FilterPrice = styled.div`
    width: 100%;
    height: 40px;

    display: flex;
    justify-content: flex-end;
    align-items: center;

    border-top: 1px solid #e5e5e5;
    border-bottom: 1px solid #e5e5e5;

    margin-top: 1%;
    margin-bottom: 1%;


    select{
        height: 60%;
        width: 15%;

        border-radius: 4px;
        border: 1px solid #4d4d4d;

        background-color: transparent;

        @media(max-width: 768px){
            height: 80%;
            width: 50%;
        }
    }

    h5{
        margin-right: 1%;
        color: #4d4d4d;
    }
`;

export const ProductList = styled.div`
    min-height: 80%;
    width: 100%;

    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    align-self: center;
`;