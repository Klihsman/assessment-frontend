import React, {useState, useEffect} from 'react';
import { CatalogoContainer, FilterContainer, FilterPrice, ProductList, ProductsContainer } from './styles';

import Card from '../Card';

const Catalogo = ({products,  category, filter, active}) =>{
    const [list, setList] = useState([]);
    const [typeFilter, setTypeFilter] =  useState([]);


    function sortProducts(type) {
        const localProducts = [...products]

        if(type === 'Todos'){
            setList(
                localProducts.sort((a, b)=>{
                    if(a.id > b.id){
                        return 1;
                    }
                    if(a.id < b.id){
                        return -1
                    }
                    return 0;
                }))
        }

         if(type === 'Maior Preço'){
             setList(
                localProducts.sort((a, b)=>{
                    if(a.price < b.price){
                        return 1;
                    }
                    if(a.price > b.price){
                        return -1
                    }
                    return 0;
                })
            )
        }

        if(type === 'Menor Preço'){
            setList(
             localProducts.sort((a, b)=>{
                if(a.price > b.price){
                    return 1;
                }
                if(a.price < b.price){
                    return -1
                }
                return 0;
            }))
        }
    }

    useEffect(()=>{
        setList(products)
        const filterArray = products.map(product => {
            return Object.values(product.filter[0])[0]
        });
        const temp = [];
        filterArray.forEach(item => {
            if(!temp.includes(item) && temp.length < 3){
                temp.push(item);
            }
        })
        setTypeFilter(temp);
    },[products])


   return(
       <CatalogoContainer>

            <FilterContainer>
                <h1>Filtre por</h1>
                <h2>Categorias</h2>
                <ul>
                    {category.map(item => <li>{item.name}</li>)}
                </ul>
                {filter.map(item => <h3>{item[Object.keys(item)]}</h3>)}
                <ul>
                {typeFilter.map(item => <li>{item}</li>)}
                </ul>
            </FilterContainer>

            <ProductsContainer>
                <h2>{active}</h2>
                <FilterPrice>
                    <h5>Ordenar por:</h5>
                    <select onChange={e => {sortProducts(e.target.value)}}>
                        <option value="Todos">Todos</option>
                        <option value="Maior Preço">Maior Preço</option>
                        <option value="Menor Preço">Menor Preço</option>
                    </select>
                </FilterPrice>
                <ProductList>
                    {list.map((product) => <Card key={product.id} name={product.name} image={product.image} price={product.price}/>)}
                </ProductList>
            </ProductsContainer>

       </CatalogoContainer>
   )
} 

export default Catalogo;