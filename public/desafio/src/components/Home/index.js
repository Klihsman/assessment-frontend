import React, {useEffect, useState} from 'react';
import axios from 'axios'

import {
    HomeContainer, 
    Header, 
    MenuContainer, 
    SearchContainer, 
    Menu, 
    Option,
    Input,
    SearchButton,
    BodyContainer,
    FilterContainer,
    TextContainer,
    DescriptionContainer,
    BackgroundContainer,
    Footer,
    Account,
} from './styles';

import logo from '../../assets/webjump.jpeg';
import Catalogo from '../Catalogo';

const Home = ()=>{
    const [category, setCategory] = useState([]);
    const [product, setProduct] = useState([]);
    const [filter, setFilter] = useState([]);
    const [status, setStatus] = useState(false);
    const [active, setActive] = useState('');



    async function getCategory(){
        await axios.get('http://localhost:8888/api/V1/categories/list')
        .then((response) => {
            setCategory(response.data.items)
        })
        .catch(error => {
            alert(error)
        })
    }


    async function getProduct(id){
        await axios.get(`http://localhost:8888/api/V1/categories/${id}`)
        .then((response) => {
            setFilter(response.data.filters)
            setProduct(response.data.items);
        })
        .catch(error => {
            alert(error)
        })
    }


    function verifyExibition(itemId, itemCategory) {
        getProduct(itemId);
        setActive(itemCategory);
        setStatus(true);
    }

    useEffect(()=>{
        getCategory();
    }, []);

    return(
        <HomeContainer>
            <Account><a href='/'>Acesse sua conta</a> ou <a href='/'>Cadastre-se</a></Account>
            <Header>

                <SearchContainer>

                    <img src={logo} alt="logo"/>

                    <div>
                        <Input placeholder="Pesquisar" />
                        <SearchButton>Buscar</SearchButton>
                    </div>

                </SearchContainer>

                <MenuContainer>
                    <Menu>
                        <Option onClick={()=> setStatus(false)}>Página Inicial</Option>
                        {category.map(item => <Option onClick={()=> verifyExibition(item.id, item.name)} key={item.name}>{item.name}</Option>)}
                        <Option>Contato</Option>
                    </Menu>
                </MenuContainer>
            </Header>

            <BodyContainer>
                {status ? <Catalogo 
                active={active} 
                filter={filter} 
                category={category} 
                products={product} 
                changeFilter={setProduct}
                />
                :
                <>
                <FilterContainer>
                    <ul>
                        <li>
                            <a href="/">Página Inicial</a>
                        </li>

                        {category.map(item => 
                            <li key={item.name}>
                                <span onClick={()=> verifyExibition(item.id, item.name)}>{item.name}</span>
                            </li>
                            )}
                        <li>
                            <a href="/">Contato</a>
                        </li>
                    </ul>
                </FilterContainer>

                <TextContainer>
                    <BackgroundContainer />
                    <DescriptionContainer>
                        <h1>Seja bem-vindo</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Risus feugiat in ante metus dictum at. Sagittis id consectetur purus ut faucibus pulvinar. Vivamus at augue eget arcu dictum varius. Duis convallis convallis tellus id interdum velit laoreet id donec. Sit amet massa vitae tortor. Diam vulputate ut pharetra sit amet aliquam. Quam elementum pulvinar etiam non quam lacus suspendisse faucibus interdum. Mollis nunc sed id semper risus in. Mauris a diam maecenas sed enim ut sem viverra. Laoreet sit amet cursus sit amet dictum. Elit scelerisque mauris pellentesque pulvinar pellentesque. Risus feugiat in ante metus dictum at tempor commodo ullamcorper. Cras adipiscing enim eu turpis. Consectetur adipiscing elit duis tristique sollicitudin nibh. Ut ornare lectus sit amet est. Fermentum dui faucibus in ornare quam viverra orci. Aliquam ultrices sagittis orci a. Tellus id interdum velit laoreet id donec ultrices tincidunt arcu. Quis varius quam quisque id diam.</p>
                    </DescriptionContainer>
                </TextContainer> 
                </>
                }
            </BodyContainer>

            <Footer />
        </HomeContainer>
    )
}

export default Home;