import styled from 'styled-components';

export const HomeContainer = styled.div`
    min-height: 130vh;
    width: 100%;

    display: flex;
    align-items: center;
    flex-direction: column;
`;

export const Account = styled.div`
    width: 100%;

    background-color: #4d4d4d;
    color: #fff;

    text-align: right;
    padding-right: 1%;

    a{
        color #fff;
    }

    @media(max-width: 768px){
        text-align: center;
        font-size: 1.2em;
    }
`;

export const Header = styled.div`
    height: 30vh;
    width: 100%;

    @media(max-width: 768px){
        height: 20vh;
    }
`;

export const SearchContainer = styled.div`
    height: 70%;
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: space-between;

    img{
        max-height: 60%;


        @media(max-width: 768px){
            max-height: 70%;
        }
    }

    div{
        height: 30%;
        width: 40%;

        display: flex;
        align-items: center;
        justify-content: center;

        margin-right: 1%;

        @media(max-width: 768px){
            width: 95%
        }
    }
    
    @media(max-width: 768px){
            align-items: center;
            justify-content: space-around;
            flex-direction: column;
        }

`;

export const Input = styled.input`
    height: 95%;
    width: 80%;

    border: 1px solid #ddd;

    padding-left: 4px;

    @media(max-width: 768px){
            height: 70%;
        }
`;

export const SearchButton = styled.button`
    height: 100%;
    width: 20%;

    background-color: #bc1d2b;

    font-weight: bold;

    color: #fff;
    cursor: pointer;

    :hover{
        filter: brightness(90%);
    }

    @media(max-width: 768px){
            height: 70%;
        }
`;

export const MenuContainer = styled.div`
    height: 30%;
    width: 100%;

    background-color: #bc1d2b;

`;

export const Menu = styled.div`
    height: 100%;
    width: 60%;

    display: flex;
    align-items: center;
    justify-content: space-around;

    @media(max-width: 768px){
        width: 100%;
        justify-content: space-around;
    }
`;

export const Option = styled.button`
    height: 100%;
    width: 18%;

    font-weight: bold;
    color: #fff;

    background-color: transparent;
    cursor: pointer;

    :hover{
        filter: brightness(90%);
    }

    @media(max-width: 768px){
        width: auto;

        font-size: 0.7em;
    }
`;

export const BodyContainer = styled.div`
    min-height: 80vh;
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: space-around;
`;

export const FilterContainer = styled.div`
    height: 70vh;
    width: 20%;

    background-color: #ddd;

    @media(max-width: 768px){
        display: none;
    }


    ul{
        margin-left: 15%;

        li{
            margin-top: 5%;

            font-size: 1.2em;

            text-decoration: none;
            color: #000;

                :hover{
                    text-decoration: underline;
                    cursor: pointer;
                }


            a{
                text-decoration: none;
                color: #000;
                :hover{
                    text-decoration: underline;
                    cursor: pointer;
                }
            }
        }
    }
`;

export const TextContainer = styled.div`
    height: 70vh;
    width: 78%;

    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: column;

    @media(max-width: 768px){
        height: 100vh;
        width: 90%;

        margin-top: 5%;

        justify-content: center;
    }
`;

export const BackgroundContainer = styled.div`
    width: 100%;
    height: 40%;

    background-color: #e5e5e5;

    @media(max-width: 768px){
        height: 10%;
    }
`;

export const DescriptionContainer = styled.div`
    height: 55%;
    width: 100%;

    h1{
        font-weight: normal;
    }

    @media(max-width: 768px){
        height: 90%;
    }
`;

export const Footer = styled.div`
    width: 99%;
    height: 19vh;

    background-color: #bc1d2b;

    align-self: center;

    margin-top: 5%;
`;