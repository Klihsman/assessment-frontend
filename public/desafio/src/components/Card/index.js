import React from 'react';
import { CardContainer } from './styles';

const Card = ({image, price, name})=>{
    
    const photo = require(`../../${image}`).default;

    return(
        <CardContainer>
            <img src={photo} alt="product"/>
            <h4>{name}</h4>
            <h5>R$ {price}</h5>
            <button>Comprar</button>
        </CardContainer>
    )
}

export default Card;