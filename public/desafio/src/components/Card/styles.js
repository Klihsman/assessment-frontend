import styled from 'styled-components';

export const CardContainer = styled.div`
    height: 300px;
    max-width: 30%;

    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: column;

    margin-bottom: 5%;

    

    img{
        border: 1px solid #ddd;

        max-height: 50%;
    }

    h4{
        color: #4d4d4d;
        font-weight: 500;
    }

    h5{
        font-size: 1.3em;
        color: #bc1d2b;
    }

    button{
        color: #fff;

        height: 30px;
        width: 150px;

        background-color: #b1c9c8;

        font-weight: 700;

        cursor: pointer;

        :hover{
            filter: brightness(90%);
        }
    }

    @media(max-width: 950px){
        max-width: 32%;
    }

    @media(max-width: 768px){
        max-width: 45%;
    }
`;