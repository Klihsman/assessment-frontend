import axios from 'axios';

const api = axios.create({
    baseUrl: "http://localhost:8888/api/V1/categories/list"
})

export default api;
